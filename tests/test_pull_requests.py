from handlers.pull_requests import get_pull_requests
from mock import patch
import unittest


class TestMergeRequests(unittest.TestCase):
    @patch('requests.get')
    def test_merge_requests(self, get_mock):

        get_mock.return_value.json.return_value = [{
            "title": "my first test",
            "number": 160897,
            "url": "https://github.com/boto/boto3/pull/160897",
            "labels": [{"name": 'needs-review',
                        "color": "f996e2"}]},
            {
            "title": "my second test",
            "number": 970816,
            "url": "https://github.com/boto/boto3/pull/970816",
            "labels": [{"name": 'needs-review',
                        "color": "f996e2"}]},
            {
            "title": "my third test",
            "number": 210922,
            "url": "https://github.com/boto/boto3/pull/210922",
            "labels": [{"name": 'bug',
                        "color": "f996e2"}]}]

        for state in ['open', 'closed', 'bug', 'needs-review']:
            print(state)
            if state == 'closed' or state == 'open':
                expected_res = [{
                    "title": "my first test",
                    "num": 160897,
                    "link": "https://github.com/boto/boto3/pull/160897"},
                    {
                    "title": "my second test",
                    "num": 970816,
                    "link": "https://github.com/boto/boto3/pull/970816"},
                    {
                    "title": "my third test",
                    "num": 210922,
                    "link": "https://github.com/boto/boto3/pull/210922"}]
            if state == 'bug':
                expected_res = [{
                    "title": "my third test",
                    "num": 210922,
                    "link": "https://github.com/boto/boto3/pull/210922"}]
            if state == 'needs-review':
                expected_res = [{
                    "title": "my first test",
                    "num": 160897,
                    "link": "https://github.com/boto/boto3/pull/160897"},
                    {
                    "title": "my second test",
                    "num": 970816,
                    "link": "https://github.com/boto/boto3/pull/970816"}]

            res = get_pull_requests(state)
            print(res)
            self.assertEqual(res, expected_res)
