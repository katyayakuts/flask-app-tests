import os
import requests

TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}


def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """

    url = 'https://api.github.com/repos/boto/boto3/pulls'

    response = requests.get(url, params={"state": state, "per_page": "100"})

    res = []

    if state == 'needs-review':
        for item in response.json():
            label = item['labels']
            for ll in label:
                if ll['name'] == 'needs-review':
                    res.append({"title": item["title"],
                                "num": item["number"],
                                "link": item["url"]})

    elif state == 'bug':
        for item in response.json():
            label = item['labels']
            for ll in label:
                if ll['name'] == 'bug':
                    res.append({"title": item["title"],
                                "num": item["number"],
                                "link": item["url"]})

    else:
        for item in response.json():
            res.append({"title": item["title"],
                        "num": item["number"],
                        "link": item["url"]})

    return res
